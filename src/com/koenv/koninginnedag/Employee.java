package com.koenv.koninginnedag;

import android.content.Context;

public class Employee {
	
	String _id;
	String _name;
	double _age;
	int _dept;
	boolean _sold = false;
	
	public Employee(String Name,double Age,int Dept)
	{
		
		this._name=Name;
		this._age=Age;
		this._dept=Dept;
	}
	
	public Employee(String Name,double Age,int Dept, String ID)
	{
		
		this._name=Name;
		this._age=Age;
		this._dept=Dept;
		this._id=ID;
	}
	
	public Employee(String Name,double Age,int Dept, String ID, boolean Sold)
	{
		
		this._name=Name;
		this._age=Age;
		this._dept=Dept;
		this._id=ID;
		this._sold=Sold;
	}
	
	public Employee(String Name,double Age)
	{
		this._name=Name;
		this._age=Age;
	}
	
	public String getID()
	{
		return this._id;
	}
	public void SetID(String ID)
	{
		this._id=ID;
	}
	
	public String getName()
	{
		return this._name;
	}
	
	public double getAge()
	{
		return this._age;
	}
	
	public boolean getSold()
	{
		return this._sold;
	}
	
	public Integer getSoldInt()
	{
		Integer Sold = 0;
		if (this._sold == true)
		{
			Sold = 1;
		}
		return Sold;
	}
	
	public void setName(String Name)
	{
		this._name=Name;
	}
	public void setAge(Double double1)
	{
		this._age=double1;
	}
	public void setSold(boolean Sold)
	{
		this._sold=Sold;
	}
	
	
	
	public void setDept(int Dept)
	{
		this._dept=Dept;
	}
	
	public String getDeptName(Context con, int Dept)
	{
		return new DatabaseHelper(con).GetDept(Dept);
	}
	public int getDept()
	{
		return this._dept;
	}
}
