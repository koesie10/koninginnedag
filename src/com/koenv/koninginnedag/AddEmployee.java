package com.koenv.koninginnedag;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Spannable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class AddEmployee extends Activity {
	EditText txtName;
	EditText txtAge;
	EditText txtID;
	TextView txtEmps;
	DatabaseHelper dbHelper;
	Spinner spinDept;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addemployee);
        txtName=(EditText)findViewById(R.id.txtName);
        txtAge=(EditText)findViewById(R.id.txtAge);
        txtID=(EditText)findViewById(R.id.txtIDAdding);
        txtEmps=(TextView)findViewById(R.id.txtEmps);
        spinDept=(Spinner)findViewById(R.id.spinDept);
    }
	
	@Override
	public void onStart()
	{
		try
		{
		super.onStart();
		dbHelper=new DatabaseHelper(this);
		//txtEmps.setText(txtEmps.getText()+String.valueOf(dbHelper.getEmployeeCount()));
		txtEmps.setText(getString(R.string.number_of_goods)+ " " +String.valueOf(dbHelper.getEmployeeCount()));
		Cursor c=dbHelper.getAllDepts();
		startManagingCursor(c);
		
		
		
		//SimpleCursorAdapter ca=new SimpleCursorAdapter(this,android.R.layout.simple_spinner_item, c, new String [] {DatabaseHelper.colDeptName}, new int []{android.R.id.text1});
		SimpleCursorAdapter ca=new SimpleCursorAdapter(this,R.layout.deptspinnerrow, c, new String [] {DatabaseHelper.colDeptName,"_id"}, new int []{R.id.txtDeptName});
		//ca.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinDept.setAdapter(ca);
		spinDept.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View selectedView,
					int position, long id) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		//never close cursor
		}
		catch(Exception ex)
		{
			CatchError(ex.toString());
		}
	}
	
	public void btnAddEmp_Click(View view)
	{
		boolean ok=true;
		try
		{
			Spannable spn = null;
			String name = null;
			String ID = null;
			double age = 0.00;
			int deptID = 0;
			try
			{
				spn=txtAge.getText();
				name=txtName.getText().toString();
				ID = String.valueOf(txtID.getText().toString());
				age=Double.valueOf(spn.toString());
				deptID=Integer.valueOf((int)spinDept.getSelectedItemId());
			}
			catch (Exception e)
			{
				CatchError(getString(R.string.unable_parsing) + " " + getString(R.string.empty_field));
			}
			if (spn == null || name == null || ID == null || age == 0.00 || deptID == 0)
			{
				throw new Exception();
			}
			Employee emp=new Employee(name,age,deptID, ID);
			
			dbHelper.AddEmployee(emp);
			
			txtAge.setText("");
			txtName.setText("");
			txtID.setText("");
			
		}
		catch(Exception ex)
		{
			ok=false;
			CatchError(ex.toString());
		}
		finally
		{
			if(ok)
			{
				//NotifyEmpAdded();
				Alerts.ShowEmpAddedAlert(this);
				txtEmps.setText(getString(R.string.number_of_goods)+" "+String.valueOf(dbHelper.getEmployeeCount()));
			}
		}
	}
	
	void CatchError(String Exception)
	{
		Dialog diag=new Dialog(this);
		diag.setTitle(getString(R.string.add_new_good));
		TextView txt=new TextView(this);
		txt.setText(Exception);
		diag.setContentView(txt);
		diag.show();
	}
	
	void NotifyEmpAdded()
	{
		Dialog diag=new Dialog(this);
		diag.setTitle(getString(R.string.add_new_good));
		TextView txt=new TextView(this);
		txt.setText(getString(R.string.good_added_success));
		diag.setContentView(txt);
		diag.show();
		try {
			diag.wait(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			CatchError(e.toString());
		}
		diag.notify();
		diag.dismiss();
	}
	
	public void startScan()
    {
    	IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan();
    }
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
  	  IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
  	  if (scanResult != null) {
  	    // handle scan result
  		  txtID.setText(scanResult.getContents());
  	  }
  	  // else continue with any other code you need in the method
  	  
  	}
	public void scanButton(View view) {
	     startScan();
	 }
	
}
