package com.koenv.koninginnedag;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class SearchActivity extends ListActivity {
	DatabaseHelper dbHelper;
	Cursor All;
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.listview);
	    handleIntent(getIntent());
	}

	@Override
	protected void onNewIntent(Intent intent) {
	    setIntent(intent);
	    handleIntent(intent);
	}

	private void handleIntent(Intent intent) {
	    if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
	      String query = intent.getStringExtra(SearchManager.QUERY);
	      SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, SearchProvider.AUTHORITY, SearchProvider.MODE);
	      suggestions.saveRecentQuery(query, null);
	      search(query);
	    }
	}
	private void search(String query)
	{
		dbHelper = new DatabaseHelper(this);
		All = dbHelper.getSearch(query);
		int[] to = new int[] { R.id.name_entry, R.id.price_entry };
		String[] columns = new String[] { DatabaseHelper.colName, DatabaseHelper.colAge };
		int[] colors = dbHelper.getColors();
		SimpleCursorAdapter adapter = new SpecialAdapter(this, R.layout.list_item, All, columns, to, colors );
		setListAdapter(adapter);
		ListView lv = getListView();
		lv.setTextFilterEnabled(true);
	}
	
	/* GENERAL */
	String query = null;
	@Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }
	public boolean onOptionsItemSelected(MenuItem item)
    {
    	switch (item.getItemId()) {
    	//Add employee
    	case R.id.menu_add:
    		Intent addIntent=new Intent(this,AddEmployee.class);
    		startActivity(addIntent);
    		return true;
    	case R.id.menu_refresh:
    		GridListView.All.requery();
    		return true;
    	case R.id.menu_search:
    		onSearchRequested();
    		return true;
    	case R.id.menu_search_barcode:
    		startScan();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
	
	public void startScan()
    {
    	IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan();
    }
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
  	  IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
  	  if (scanResult != null) {
  		  query = scanResult.getContents();
  		  onSearchRequested();
  	  }
	}
	@Override
    public boolean onSearchRequested() {
    	startSearch(query, false, null, false);
    	query = null;
        return false;
    }
	
	/* END GENERAL */
}
