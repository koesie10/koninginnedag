package com.koenv.koninginnedag;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class GridList extends Activity {
	DatabaseHelper dbHelper;
	static public GridView grid;
	TextView txtTest;
	TextView txtID;
	Spinner spinDept1;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        setContentView(R.layout.gridview);
        grid=(GridView)findViewById(R.id.grid);
        txtTest=(TextView)findViewById(R.id.txtTest);
        spinDept1=(Spinner)findViewById(R.id.spinDept1);
        
        // Capture our button from layout
        txtID = (TextView)findViewById(R.id.txtIDAdding);
        // Register the onClick listener with the implementation above
        //txtID.setOnClickListener(BarcodeClickListener);
        
        Utilities.ManageDeptSpinner(this.getParent(),spinDept1);
        final DatabaseHelper db=new DatabaseHelper(this);
        try
        {
         
         spinDept1.setOnItemSelectedListener(new OnItemSelectedListener() {
        	 
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				LoadGrid();
	    		//sca.notifyDataSetChanged();
	    		
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
       
        }
        catch(Exception ex)
        {
        	txtTest.setText(ex.toString());
        }
        
        
       
        try
        {
        grid.setOnItemClickListener(new OnItemClickListener()
        {

        	@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
					long id) {
				// TODO Auto-generated method stub
				try
				{
			
				SQLiteCursor cr=(SQLiteCursor)parent.getItemAtPosition(position);
				String name=cr.getString(cr.getColumnIndex(DatabaseHelper.colName));
				double age=cr.getDouble(cr.getColumnIndex(DatabaseHelper.colAge));
				String Dept=cr.getString(cr.getColumnIndex(DatabaseHelper.colDeptName));
				String ID=cr.getString(cr.getColumnIndex(DatabaseHelper.colID));
				Employee emp=new Employee(name, age,db.GetDeptID(Dept), ID);
				//emp.SetID((int)id);
				AlertDialog diag= Alerts.ShowEditDialog(GridList.this,emp);
				diag.setOnDismissListener(new OnDismissListener() {
					
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						txtTest.setText(getString(R.string.dismissed));
						//((SimpleCursorAdapter)grid.getAdapter()).notifyDataSetChanged();
						LoadGrid();
					}
				});
				diag.show();
				}
				catch(Exception ex)
				{
					Alerts.CatchError(GridList.this, ex.toString());
				}
			}

			
        }
        );
        }
        catch(Exception ex)
        {
        	
        }

    }
    
    @Override
    public void onStart()
    {
    	super.onStart();
    	//LoadGrid();
    }
    
    public void LoadGrid()
    {
    	dbHelper=new DatabaseHelper(this);
    	try
    	{
    		//Cursor c=dbHelper.getAllEmployees();
    		View v=spinDept1.getSelectedView();
			TextView txt=(TextView)v.findViewById(R.id.txtDeptName);
			String Dept=String.valueOf(txt.getText());
    		Cursor c=dbHelper.getEmpByDept(Dept);
    		startManagingCursor(c);
    		
    		String [] from=new String []{DatabaseHelper.colName,DatabaseHelper.colAge,DatabaseHelper.colDeptName};
    		int [] to=new int [] {R.id.colName,R.id.colAge,R.id.colDept};
    		SimpleCursorAdapter sca=new SimpleCursorAdapter(this,R.layout.gridrow,c,from,to);
    		grid.setAdapter(sca);
    		
    		
    		
    	}
    	catch(Exception ex)
    	{
    		AlertDialog.Builder b=new AlertDialog.Builder(this);
    		b.setMessage(ex.toString());
    		b.show();
    	}
    }
    
    public void startScan()
    {
    	IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan();
    }
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
  	  IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
  	  if (scanResult != null) {
  	    // handle scan result
  		  txtID.setText(scanResult.getContents());
  	  }
  	  // else continue with any other code you need in the method
  	  
  	}
	public void scanButtonEdit(View view) {
	     startScan();
	 }
}
