package com.koenv.koninginnedag;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;

public class SpecialAdapter extends SimpleCursorAdapter {
	int[] COLORS = null;

	public SpecialAdapter(Context context, Integer layout, Cursor cursor, String[] from, int[] to, int[] colors) {
		super(context, layout, cursor, from, to);
		COLORS = colors;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);
		view.setBackgroundColor(COLORS[position]);
		return view;
	}
}
