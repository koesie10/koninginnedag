package com.koenv.koninginnedag;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.os.Bundle;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class GridListView extends ListActivity {
	DatabaseHelper dbHelper;
	static Cursor All;
	//public int selectedItem = -1;
	protected Object mActionMode;
	SimpleCursorAdapter adapter = null;
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		  super.onCreate(savedInstanceState);
		  setContentView(R.layout.listview);
		  dbHelper = new DatabaseHelper(this);
		  All = dbHelper.getAllEmployees();
		  int[] to = new int[] { R.id.name_entry, R.id.price_entry };
		  String[] columns = new String[] { DatabaseHelper.colName, DatabaseHelper.colAge };
		  int[] colors = dbHelper.getColors();
		  adapter = new SpecialAdapter(this, R.layout.list_item, All, columns, to, colors );
		  setListAdapter(adapter);
	
		  ListView lv = getListView();
		  lv.setTextFilterEnabled(true);
		  registerForContextMenu(lv); 
	 }
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		try
		{
		final DatabaseHelper db=new DatabaseHelper(this);
		SQLiteCursor cr=(SQLiteCursor)l.getItemAtPosition(position);
		String name=cr.getString(cr.getColumnIndexOrThrow(DatabaseHelper.colName));
		double age=cr.getDouble(cr.getColumnIndexOrThrow(DatabaseHelper.colAge));
		String Dept=cr.getString(cr.getColumnIndexOrThrow(DatabaseHelper.colDeptName));
		String ID=cr.getString(cr.getColumnIndexOrThrow("_id"));
		Integer Sold_temp=cr.getInt(cr.getColumnIndexOrThrow(DatabaseHelper.colSold));
		Boolean Sold = false;
		if (Sold_temp==1)
		{
			Sold = true;
		}
		Employee emp=new Employee(name, age,db.GetDeptID(Dept), ID, Sold);
		AlertDialog diag= Alerts.ShowEditDialog(GridListView.this,emp);
		diag.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				onCreate(null);
			}
		});
		diag.show();
		onCreate(null);
		}
		catch(Exception ex)
		{
			//Alerts.CatchError(GridListView.this, ex.toString());
			Alerts.CatchError(GridListView.this, ex.getMessage());
		}
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.context_menu, menu);
	}
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	    AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    //info.id;
	    Object item_new = adapter.getItemId(info.position);
	    Cursor employee = adapter.getCursor();
	    employee.moveToPosition(info.position);
	    Integer colID = employee.getColumnIndexOrThrow("_id");
	    String ID = employee.getString(colID);
	    Integer colName = employee.getColumnIndexOrThrow(DatabaseHelper.colName);
	    String Name = employee.getString(colName);
	    Integer colPrice = employee.getColumnIndexOrThrow(DatabaseHelper.colAge);
	    Double Price = employee.getDouble(colPrice);
	    Integer colDeptName = employee.getColumnIndexOrThrow(DatabaseHelper.colDeptName);
	    String Dept = employee.getString(colDeptName);
	    Integer colSold = employee.getColumnIndexOrThrow(DatabaseHelper.colSold);
	    Integer Sold_temp = employee.getInt(colSold);
	    Boolean Sold = false;
	    if (Sold_temp == 1)
	    {
	    	Sold = true;
	    }
	    Employee emp = new Employee(Name, Price, dbHelper.GetDeptID(Dept), ID, Sold);
	    switch (item.getItemId()) {
	        case R.id.context_delete:
	        	Log.i("AndroidRuntime",item_new.toString());
	        	dbHelper.DeleteEmp(emp);
	        	All.requery();
	            return true;
	        case R.id.context_sell:
	        	if (emp.getSold() == true)
	        	{
	        		emp.setSold(false);
	        	}
	        	else
	        	{
	        		emp.setSold(true);
	        	}
	        	dbHelper.UpdateEmp(emp);
	        	//All.requery();
	        	onCreate(null);
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}
	
	/* GENERAL */
	String query = null;
	@Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }
	public boolean onOptionsItemSelected(MenuItem item)
    {
    	switch (item.getItemId()) {
    	//Add employee
    	case R.id.menu_add:
    		Intent addIntent=new Intent(this,AddEmployee.class);
    		startActivity(addIntent);
    		return true;
    	case R.id.menu_refresh:
    		GridListView.All.requery();
    		onCreate(null);
    		return true;
    	case R.id.menu_search:
    		onSearchRequested();
    		return true;
    	case R.id.menu_search_barcode:
    		startScan();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
	
	public void startScan()
    {
    	IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan();
    }
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
  	  IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
  	  if (scanResult != null) {
  		  query = scanResult.getContents();
  		  onSearchRequested();
  	  }
	}
	@Override
    public boolean onSearchRequested() {
    	startSearch(query, false, null, false);
    	query = null;
        return false;
    }
	
	/* END GENERAL */
	
}
