package com.koenv.koninginnedag;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

public class Alerts {
	//private Context context = this;
public static void ShowEmpAddedAlert(Context con)
{
	AlertDialog.Builder builder=new AlertDialog.Builder(con);
	builder.setTitle(con.getString(R.string.add_good));
	builder.setIcon(android.R.drawable.ic_dialog_info);
	DialogListner listner=new DialogListner();
	builder.setMessage(con.getString(R.string.good_added_success));
	builder.setPositiveButton(con.getString(R.string.ok), listner);
	
	AlertDialog diag=builder.create();
	diag.show();
}

public static AlertDialog ShowEditDialog(final Context con,final Employee emp)
{
	AlertDialog.Builder b=new AlertDialog.Builder(con);
	b.setTitle("Good Details");
	LayoutInflater li=LayoutInflater.from(con);
	View v=li.inflate(R.layout.editdialog, null);
	
	b.setIcon(android.R.drawable.ic_input_get);
	
	b.setView(v);
	final TextView txtName=(TextView)v.findViewById(R.id.txtDelName);
	final TextView txtAge=(TextView)v.findViewById(R.id.txtDelAge);
	final TextView txtID = (TextView)v.findViewById(R.id.txtDelID);
	final CheckBox checkBox = (CheckBox) v.findViewById(R.id.txtCheckbox);
	final Spinner spin=(Spinner)v.findViewById(R.id.spinDiagDept);
	Utilities.ManageDeptSpinner(con, spin);
	for(int i=0;i<spin.getCount();i++)
	{
		long id=spin.getItemIdAtPosition(i);
		if(id==emp.getDept())
		{
			spin.setSelection(i, true);
			break;
		}
	}
	
	
	txtName.setText(emp.getName());
	txtAge.setText(String.valueOf(emp.getAge()));
	txtID.setText(String.valueOf(emp.getID()));
	checkBox.setChecked(emp.getSold());
	
	b.setPositiveButton(con.getString(R.string.modify), new OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			emp.setName(txtName.getText().toString());
			emp.setAge(Double.valueOf(txtAge.getText().toString()));
			emp.setDept((int)spin.getItemIdAtPosition(spin.getSelectedItemPosition()));
			emp.SetID(String.valueOf(txtID.getText().toString()));
			emp.setSold(checkBox.isChecked());
			
			try
			{
			DatabaseHelper db=new DatabaseHelper(con);
			db.UpdateEmp(emp);
			GridListView.All.requery();
			
			}
			catch(Exception ex)
			{
				CatchError(con, ex.toString());
			}
		}
	});
	
	b.setNeutralButton(con.getString(R.string.delete), new OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			DatabaseHelper db=new DatabaseHelper(con);
			db.DeleteEmp(emp);
		}
	});
	b.setNegativeButton(con.getString(R.string.cancel), null);
	
	return b.create();
	//diag.show();
	
}

static public void CatchError(Context con, String Exception)
{
	Dialog diag=new Dialog(con);
	diag.setTitle(con.getString(R.string.error));
	TextView txt=new TextView(con);
	txt.setText(Exception);
	diag.setContentView(txt);
	diag.show();
}

}


