package com.koenv.koninginnedag;




import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.app.TabActivity;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;




import android.widget.GridView;

import android.widget.TabHost;
import android.widget.TextView;


/* NOT USED ANYMORE BECAUSE OF DEPRECECATION TabActivity */
public class DatabaseDemo extends TabActivity {
	DatabaseHelper dbHelper;
	GridView grid;
	TextView txtTest;
	String query = null;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        SetupTabs();

    }
    
    
    
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }
    
    @Override
    public boolean onSearchRequested() {
    	startSearch(query, false, null, false);
    	query = null;
        return false;
    }
    
    
    
    
    
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	switch (item.getItemId()) {
    	//Add employee
    	case R.id.menu_add:
    		Intent addIntent=new Intent(this,AddEmployee.class);
    		startActivity(addIntent);
    		return true;
    	case R.id.menu_refresh:
    		GridListView.All.requery();
    		return true;
    	case R.id.menu_search:
    		onSearchRequested();
    		return true;
    	case R.id.menu_search_barcode:
    		startScan();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
    
    void SetupTabs()
    {

    	TabHost host=getTabHost();

        TabHost.TabSpec spec=host.newTabSpec("tag1");
        Intent in1=new Intent(this, AddEmployee.class);
        spec.setIndicator(getString(R.string.add_good));
        spec.setContent(in1);
        
        
        
        TabHost.TabSpec spec2=host.newTabSpec("tag2");
        //Intent in2=new Intent(this, GridList.class);
        Intent in2=new Intent(this, GridListView.class);
        
        spec2.setIndicator(getString(R.string.goods));
        spec2.setContent(in2);
        
        host.addTab(spec2);
        host.addTab(spec);
        
       
    }
    
    public void startScan()
    {
    	IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan();
        Log.i("AndroidRuntime", "started");
    }
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
	Log.i("AndroidRuntime", "Received");
  	  IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
  	  if (scanResult != null) {
  	    // handle scan result
  		  //appSearchData.putString(scanResult.getContents(), DatabaseDemo.class.getName());
  		  query = scanResult.getContents();
  		  onSearchRequested();
  		  Log.i("AndroidRuntime", "true");
  		  //startSearch(scanResult.getContents(), false, appSearchData, false);
  	  }
  	  else
  	  {
  		  Log.i("AndroidRuntime", "false");
  	  }
  	  // else continue with any other code you need in the method
  	  
  	}
    
}